# DEPLOYMENT

### Hello. We are going to go over the process of deploying the API.

#### Pulling the Respository
You are going to want to have all the files on hand to be able to run the scripts 
and have the data on hand for when you run the respository to be able to have a successful run.
### ---------------------------------------------------------------------------------
```
git clone git@bitbucket.org:dee467/coe332-project-austin-animals.git
```
### ----------------------------------------------------------------------------------


#### Download the Docker Images from Docker Hub
We will need the Images to proceed with the running of the api. So we can either build them from 
source or pull the image from Docker Hub. Doing the later. We will assume that Docker is installed
on your local computer,else you will need it installed. 

##### Go Here to Install Docker
https://docs.docker.com/install/

##### For the API image
```
docker pull danielesr/lunarvacation
```
#### Launching the Containers
Launching the api server and the worker service is done by constructing containers 
from the images previously pulled from Docker Hub and using the docker-compose.yml 
files in the repository. You might also need to install docker-compose to run this.

```
docker-compose up -d 
```

##### After all this YOU can finally interact with the API !!!!
### Done
