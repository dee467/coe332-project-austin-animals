### USER GUIDE and Minor Documentation

#### USING CURL
```
localhost:5000
```

##### curl localhost:5000/

``` 
	{    
		"Age upon Intake": "8 years",
        "Animal ID": "A725711",
        "Animal Type": "Dog",
        "Breed": "Labrador Retriever Mix",
        "DateTime": "05/02/2016 11:36:00 AM",
        "Name": "Boone",
        "Sex upon Intake": "Intact Male"
    },
    {
        "Age upon Intake": "2 years",
        "Animal ID": "A681642",
        "Animal Type": "Dog",
        "Breed": "Chihuahua Shorthair Mix",
        "DateTime": "09/02/2014 12:42:00 PM",
        "Name": "Honey",
        "Sex upon Intake": "Spayed Female"
    }
```
| Endpoint | Method | Description | Example Curl
| -------- | ------ | ----------- | -------------|
| /| GET    | Used to get all data on animals.|
| /id/{ID} | GET | Get data corresponding to the animal with animal ID {ID}. |
| /type/{type} | GET | Get animals on any animals of type {type}. Options are dog, cat, or other. | curl localhost:5000/type/Dog
| /type/{type}/name/{name} | GET | Get data on animals of type {type} named {name}. | curl localhost:5000/type/Dog/name/honey
| /type/{type}/breed/{breed} | GET | Get data on animals of type {type} and breed {breed}. | curl localhost:5000/type/dog/breed/pointer
| /type/{type}/breed/{breed}/age/{age} | GET | Get data on animals of type {type}, breed {breed}, and age {age}. 6 months is wrtten as 6%20months; 6 years is wrtten as 6%20years |  curl localhost:5000/type/cat/breed/shorthair/age/6%20months
| /type/{type}/age/{age} | GET | Get data on animals of type {type} and age {age}.6 months is wrtten as 6%20months; 6 years is wrtten as 6%20years| curl localhost:5000/type/cat/age/6%20months
| /type/{type}/sex/{sex} | GET | Get data on animals of type {type} and sex {sex}. Options for sex are spayed/intact female', neutered/intact male,  and 'unknown' | curl localhost:5000/type/dog/sex/intact%20male
| /date/{year} | GET | Get data on animals found in year {year}. | curl localhost:5000/date/2015
| /recent | GET | Most recent animal intake | curl localhost:5000/recent
| /range/{start_year.start_month}/{end_year.end_month} | GET| Get all data on animals found between start_month, start_year and end_month, end_year. For example, to see all animals found between February 2014 and August 2016, use /range/2014.2/2016.8 . | curl localhost:5000/range/2015.1/2016.1
| /jobs | GET | Get all jobs in the redis database | curl localhost:5000/jobs
| /jobs | POST| Post a job to the redis data base, get plots and average from one year to the next. Entries must be strings | curl -d '{"start":"2015","end":"2016"}' localhost:5000/jobs 
| /jobs/{jid}/plot | GET | Saves a plot of the the number of animals intook per month in a png file.* NOTE jid is different each time, list all jobs and copy whichever job id you need instad of relying on example | curl localhost:5000/jobs/293abe43-9794-405f-9e38-b5faede0cb1a/plot --output plot.png
| /jobs/{jid}/mean | GET | Display the mean number of animals intook per month for that time period in job. * NOTE jid is different each time, list all jobs and copy whichever job id you need instad of relying on example | curl localhost:5000/jobs/293abe43-9794-405f-9e38-b5faede0cb1a/mean