from uuid import uuid4
from hotqueue import HotQueue
import redis
from flask import jsonify
rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=0)
rque = HotQueue("queue",host='172.17.0.1', port=6379, db=1)
PLOT = redis.StrictRedis( host='172.17.0.1',port=6379,db=2)
means = redis.StrictRedis(host='172.17.0.1', port=6379,db=3)

def generate_jid():
    return str(uuid4())

def generate_job_key(jid):
    return 'job.{}'.format(jid)

def _instantiate_job(jid, status, start, end):
    if type(jid) == str:
        return {'id': jid,
                'status': status,
                'start': start,
                'end': end,
        }
    return {'id': jid.decode('utf-8'),
            'status': status.decode('utf-8'),
            'start': start.decode('utf-8'),
            'end': end.decode('utf-8')}

def add_job(start, end,status="submitted"):
    """Add a job to the redis queue."""
    jid = generate_jid()
    job_dict = _instantiate_job(jid, status, start, end)
    save_job(generate_job_key(jid),job_dict)
    queue_job(jid)
    return jsonify(job_dict)

def save_job(job_key, job_dict):
    """Save a job object in the Redis database."""
    rd.hmset(job_key, job_dict)

def queue_job(jid):
    rque.put(generate_job_key(jid))

def update_job_status(jid, status):
    """Update the status of job with job id `jid` to status `status`."""
    jid, status, start, end = rd.hmget(jid, 'id', 'status', 'start', 'end')
    print(jid,status,start,end)
    job = _instantiate_job(jid, status, start, end)
    if job:
        job['status'] = status
        save_job(jid, job)
    else:
        raise Exception()
def getjobs():
    joblist=[]
    for jid in rd.keys():
        j, status, start, end = rd.hmget(jid, 'id', 'status', 'start', 'end')
        job = _instantiate_job(j, status, start, end)
        joblist.append(job)     
    return jsonify(joblist)