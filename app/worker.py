import jobs
import datetime
import time
import json
from flask import jsonify,Flask,request
import pandas as pd
import matplotlib.pyplot as plt
import redis
from hotqueue import HotQueue

data = json.load(open('./AustinAnimals.json','r'))
rd = redis.StrictRedis(   host='172.17.0.1', port=6379, db=0)
rque = HotQueue("queue",  host='172.17.0.1', port=6379, db=1)
PLOT = redis.StrictRedis( host='172.17.0.1', port=6379, db=2)
means = redis.StrictRedis(host='172.17.0.1', port=6379, db=3)

def make_plot(x,y,jid):
    print(jid)
    plt.plot(x,y,20,'r')
    plt.title('Line Plot of Austin Animals for Given Time')
    plt.xlabel('Years')
    plt.ylabel('Austin Animals')
    plt.xticks(fontsize=6, rotation=75)
    plt.savefig('/tmp/doggo.png',dpi=400,figsize=(16,9))
    fb=open('/tmp/doggo.png','rb').read()
    PLOT.set(jid,fb)
    time.sleep(10)
def make_mean(jid,dates):
    mean = (sum(dates.values()))/len(dates)
    means.set(jid,mean)
    time.sleep(10)    
time.sleep(10)        
@rque.worker
def execute_job(jid):
    #update status of job to 'in progress'
    jobs.update_job_status(jid, 'in progress')
    #execute job
    jid, status, start, end = rd.hmget(jid, 'id', 'status', 'start', 'end')
    job = jobs._instantiate_job(jid, status, start, end)
    # get date range
    daterange = pd.date_range(job['start'],job['end'],freq='M')
    daterange = daterange.strftime('%m/%Y')
    d={}
    for months in daterange:
        d[months]=0
    for dog in data:
        date=pd.to_datetime(dog['DateTime'])
        if date.strftime('%m/%Y') in d.keys():
            d[date.strftime('%m/%Y')]+=1
    make_mean(job['id'],d)
    y=list(d.values())
    x=list(d.keys())
    make_plot(x,y,job['id'])
    time.sleep(5)
    #update status of job to 'complete'
    jobs.update_job_status(jid,'complete')
execute_job()

