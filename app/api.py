import json	
from flask import Flask,request,jsonify,abort,send_file
from datetime import datetime
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import redis
import jobs
from hotqueue import HotQueue 
import requests
import io
import matplotlib.image as mpimg

rd = redis.StrictRedis(host='172.17.0.1', port=6379, db=0)
rque = HotQueue("queue", host='172.17.0.1', port=6379, db=1)
PLOT = redis.StrictRedis(host='172.17.0.1',port=6379,db=2)
means = redis.StrictRedis(host='172.17.0.1', port=6379,db=3)

app= Flask(__name__)

data= json.load(open('./AustinAnimals.json', 'r'))

@app.route('/')
def animals():
    return jsonify(data)
@app.route('/id/<id>')
def find_id(id):
	for animal in data:
		if animal["Animal ID"] == id:
			return jsonify(animal)

@app.route('/type/<type>')
def find_type(type):
	type_list = []
	for animal in data:
		# not case sensitive
		if animal["Animal Type"].lower() == type.lower():
			type_list.append(animal)
	return jsonify(type_list)

@app.route('/type/<type>/age/<age>')
def find_type_age(type,age):
	type_age_list = []
	for animal in data:
		# not case sensitive
		if animal["Animal Type"].lower() == type.lower():
			if animal["Age upon Intake"].lower() == age.lower():
				type_age_list.append(animal)
	return jsonify(type_age_list)

@app.route('/type/<type>/name/<name>')
def find_type_name(type,name):
	type_name_list = []
	for animal in data:
		if animal["Animal Type"].lower() == type.lower():
			if name.lower() in animal["Name"].lower() :
				type_name_list.append(animal)
	return jsonify(type_name_list)

@app.route('/type/<type>/sex/<sex>')
def find_type_sex(type,sex):
	type_sex_list = []
	for animal in data:
		if animal["Animal Type"].lower() == type.lower():
			if animal["Sex upon Intake"].lower() == sex.lower():
				type_sex_list.append(animal)
	return jsonify(type_sex_list)

@app.route('/type/<type>/breed/<breed>')
def find_type_breed(type,breed):
	type_breed_list= []
	for animal in data:
		if animal["Animal Type"].lower() == type.lower():
			if breed.lower() in animal["Breed"].lower():
				type_breed_list.append(animal)
	return jsonify(type_breed_list)

@app.route('/type/<type>/breed/<breed>/age/<age>')
def find_type_breed_age(type,breed,age):
	type_breed_age_list= []
	for animal in data:
		if animal["Animal Type"].lower() == type.lower():
			if breed.lower() in animal["Breed"].lower():
				if animal["Age upon Intake"].lower() == age.lower():
					type_breed_age_list.append(animal)
	return jsonify(type_breed_age_list)

@app.route('/date/<year>')
def find_year(year):
	year_list = []
	search_date = datetime.strptime(year,'%Y')
	for animal in data:
		date = datetime.strptime(animal['DateTime'].split(' ')[0],'%m/%d/%Y')
		if date.year == search_date.year:
			year_list.append(animal)
	return jsonify(year_list)

@app.route('/recent')
def find_recent():
	return jsonify(data[0])

@app.route('/range/<start>/<end>')
def find_range(start,end):
	range_list = []
	start_year, start_month = start.split('.')
	end_year, end_month = end.split('.')
	start_date = datetime.strptime(start_month+'/'+start_year,'%m/%Y')
	end_date = datetime.strptime(end_month+'/'+end_year,'%m/%Y')
	for animal in data:
		date = datetime.strptime(animal['DateTime'].split(' ')[0],'%m/%d/%Y')
		if (date>=start_date) and (date<=end_date):
			range_list.append(animal)
	return jsonify(range_list)

@app.route('/jobs', methods = ['GET','POST'])
def Jobs():
    if request.method == 'POST':
        try:
            job=request.get_json(force=True)
                # assuming everything is correct
            if int(job.get('start'))>2012 and int(job.get('end'))<2019 : 
                start=job.get('start')
                st=str(job.get('start'))
                st= "1/"+"1/"+start
                st= pd.to_datetime(st, format='%m/%d/%Y')
                job['start']=st.strftime('%m/%d/%Y')
            else :
                print(" Date out of data range ")
                return abort(404)
            if int(job.get('end'))>=int(start) and int(job.get('end'))<2019:
                end = "12/"+"31/"+str(job.get('end'))
                end = pd.to_datetime(end, format='%m/%d/%Y')
                job['end']= end.strftime('%m/%d/%Y')
            else:
                print( " Date is out of data range ")
                return abort(404)
            result = jobs.add_job(job['start'],job['end'])
            return result
        except:
            return "incorrect format for json "
    if request.method == 'GET':
    	return jobs.getjobs()

@app.route('/jobs/<jid>/plot', methods = ['GET'])
def plot(jid):
	pic= PLOT.get(jid)
	return send_file(io.BytesIO(pic),
                     mimetype='image/png',
                     as_attachment=True,
                     attachment_filename='{}.png'.format("howdy"))


@app.route('/jobs/<jid>/mean', methods = ['GET'])
def display(jid):
	return means.get(jid) 
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
