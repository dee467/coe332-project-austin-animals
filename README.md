# README #

### What is this repository for? ###

This will create a RESTful web API using Austin's Animal Center data. 
The data set includes 1000 animals that the Austin Animal Center intook since 2013. 
From this data, the user will be able to query data based on the date the animal was found, the animal type, breed, age, gender, and name. 
Data may also be determined about the number of animals found in a given month. 

### Summary
* #### Team and Contact Info
     * Braeden Conrad
        * BitBucket username: bconrad98
        * Email: *__bconrad98@gmail.com__*
     * Daniel Espinoza
        * BitBucket username: dee467
        * Email : *__danielespinoza@utexas.edu__*


### Examples

* ### Using python:
    - import requests
    - get all data on animals:
        + requests.get('localhost:5000/')
    - get all data on all dogs:
        + requests.get('localhost:5000/type/dog)
    - get data on dogs named Bella:
        + requests.get('localhost:5000/type/dog/name/Bella)

* ### Using curl:
    - get all data on animals:
        + curl localhost:5000/animals
    - get all data on all dogs:
        + curl localhost:5000/animals?type=Dog
    - get data on dogs named Bella:
        + curl localhost:5000/animals?type=Dog?name=Bella
* Each data point given will have the following format:
    - {
   "Animal ID": "A735930",
   "Name": "Bella",
   "DateTime": "10/01/2016 03:20:00 PM",
   "Animal Type": "Dog",
   "Sex upon Intake": "Intact Female",
   "Age upon Intake": "2 months",
   "Breed": "Labrador Retriever Mix"
 }
        
### Charging Plan.

* ### Using it as a Consumer.
    - FREE !!!
    - As this will be a database of potential pets, we want it to be completely free for the average person to search for their forever buddy.
    - As such they are not going to be charged for any use of this data.
* ### Using it as a Seller or Shelter
    - There will be percentage based charge on the amount of memory and pets upload by a party capping at a max percentage.
        + *__i.e__*  Austin Pets Alive may have 700 animals, but Dobie Rescue Ranch may only input 8 dogs at a time.
    - Providers will be charged per entry and a small fee for every query of theirs that a person looks at.
        + *__i.e__*  Cost will be based on traffic, and to put the animal on the server.
    - Additional
        + Look at Data Analytics over your organizations adoptions and dogs on hand.
              + Find the  average age a pet is before the organization.
              + How many pets are on hand.
              + How many of a certain breed there is (German Shepherd, Lab, ...).
        
#### UNIVERSITY OF TEXAS COE332
